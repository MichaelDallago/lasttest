#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "worker.h"

#include <QElapsedTimer>
#include <QGraphicsScene>
#include <QMainWindow>

namespace Ui {
	class MainWindow;
}

typedef QVector<QPoint> bufferType;

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = 0);
		~MainWindow();

	private slots:
		void on_startButton_clicked();
		void process(QVector<QPoint> buff);
		void update_handler();

	private:
		Ui::MainWindow *ui;
		QGraphicsScene *scene;
		QElapsedTimer timer;
		Worker *worker;
		bool stop;
};

#endif // MAINWINDOW_H
