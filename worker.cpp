#include "worker.h"

#include <QDebug>

Worker::Worker(int interval, QObject *parent) :
	QObject(parent), UPDATE_INTERVAL(interval)
{}

void Worker::doWork()
{
	qDebug() << "started";
	timer.start();
	QElapsedTimer timer2;
	timer2.start();
	for (int i = 0; i < 100000; ++i)
	{
		QPoint p(rand() % 800, rand() % 500);

		bool found = false;
		for (int j = 0; j < store.size() && !found; ++j)
			if (store[j] == p)
				found = true;

		if (!found)
		{
			store.push_back(p);
			queue(p);
		}
		else
			i--;
	}

	qDebug() << "finished in" << timer2.elapsed();
	emit finished();
}

void Worker::queue(QPoint p)
{
	buffer.push_back(p);

	if (timer.elapsed() > UPDATE_INTERVAL)
	{
		emit to_be_processed(buffer);
		buffer.clear();
		timer.restart();
	}
}
