#-------------------------------------------------
#
# Project created by QtCreator 2016-01-04T22:46:20
#
#-------------------------------------------------

QT       += core gui\
						opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lastTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    worker.cpp

HEADERS  += mainwindow.h \
    worker.h

FORMS    += mainwindow.ui

CONFIG += c++11
