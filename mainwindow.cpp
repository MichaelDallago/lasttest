#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QGLWidget>
#include <QThread>
#include <QTimer>

#define UPDATE_INTERVAL 50
#define W_UPDATE_INTERVAL 70

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	qRegisterMetaType<bufferType> ("bufferType");
	ui->setupUi(this);

	scene = new QGraphicsScene;
	scene->setSceneRect(0, 0, 800, 500);

	ui->graphicsView->setScene(scene);
	ui->graphicsView->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
	//ui->graphicsView->setViewport(new QGLWidget);
	//ui->graphicsView->setFixedSize(450, 450);
	stop = false;
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_startButton_clicked()
{
	QTimer::singleShot(UPDATE_INTERVAL, this, SLOT(update_handler()));

	worker = new Worker(W_UPDATE_INTERVAL);
	QThread *thread = new QThread;
	worker->moveToThread(thread);
	connect(worker, SIGNAL(to_be_processed(QVector<QPoint>)), this, SLOT(process(QVector<QPoint>)));
	connect(thread, SIGNAL(started()), worker, SLOT(doWork()));
	connect(worker, &Worker::finished, [&]()
	{
		qDebug() << "stop";
		stop = true;
		ui->graphicsView->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
	});
	connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
	connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();
	timer.start();
}

void MainWindow::process(QVector<QPoint> buff)
{
	for (int i = 0; i < buff.size(); ++i)
		scene->addEllipse(buff[i].x(), buff[i].y(), 1.0, 1.0);
}

void MainWindow::update_handler()
{
	scene->update();

	if (!stop)
		QTimer::singleShot(UPDATE_INTERVAL, this, SLOT(update_handler()));
}
