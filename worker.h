#ifndef WORKER_H
#define WORKER_H

#include <QElapsedTimer>
#include <QObject>
#include <QPoint>
#include <QVector>

class Worker : public QObject
{
		Q_OBJECT
	public:
		explicit Worker(int interval, QObject *parent = 0);

	signals:
		void to_be_processed(QVector<QPoint> buff);
		void finished();

	public slots:
		void doWork();

	private:
		QVector<QPoint> buffer;
		QVector<QPoint> store;
		QElapsedTimer timer;
		int UPDATE_INTERVAL;

		void queue(QPoint p);

};

#endif // WORKER_H
